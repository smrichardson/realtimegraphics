#include "MyView.hpp"
#include <SceneModel/SceneModel.hpp>
#include <tygra/FileHelper.hpp>
#include <tsl/primitives.hpp>
#include <tcf/Image.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <cassert>

MyView::
MyView() : gbuffer_position_tex_(0),
gbuffer_normal_tex_(0),
gbuffer_depth_tex_(0),
lbuffer_fbo_(0),
gbuffer_fbo_(0),
global_light_prog_(0),
point_light_prog_(0),
spot_light_prog_(0)
{
}

MyView::
~MyView() {
}

void MyView::
setScene(std::shared_ptr<const SceneModel::Context> scene)
{
	scene_ = scene;
}

void MyView::
windowViewWillStart(std::shared_ptr<tygra::Window> window)
{
	//generate light quad mesh
	{
		std::vector<glm::vec2> vertices(4);
		vertices[0] = glm::vec2(-1, -1);
		vertices[1] = glm::vec2(1, -1);
		vertices[2] = glm::vec2(1, 1);
		vertices[3] = glm::vec2(-1, 1);

		glGenBuffers(1, &light_quad_mesh_.position_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, light_quad_mesh_.position_vbo);
		glBufferData(GL_ARRAY_BUFFER,
			vertices.size() * sizeof(glm::vec2),
			vertices.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenVertexArrays(1, &light_quad_mesh_.vao);
		glBindVertexArray(light_quad_mesh_.vao);
		glBindBuffer(GL_ARRAY_BUFFER, light_quad_mesh_.position_vbo);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
			sizeof(glm::vec2), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	// generating the light sphere mesh
	{
		tsl::IndexedMesh mesh;
		tsl::CreateSphere(1.f, 12, &mesh);
		tsl::ConvertPolygonsToTriangles(&mesh);

		light_sphere_mesh_.element_count = mesh.index_array.size();
	
		glGenBuffers(1, &light_sphere_mesh_.position_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, light_sphere_mesh_.position_vbo);
		glBufferData(GL_ARRAY_BUFFER,
			mesh.vertex_array.size() * sizeof(glm::vec3),
			mesh.vertex_array.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
			
		glGenBuffers(1, &light_sphere_mesh_.element_vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_sphere_mesh_.element_vbo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			mesh.index_array.size() * sizeof(unsigned int),
			mesh.index_array.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			
		glGenVertexArrays(1, &light_sphere_mesh_.vao);
		glBindVertexArray(light_sphere_mesh_.vao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_sphere_mesh_.element_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, light_sphere_mesh_.position_vbo);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
			sizeof(glm::vec3), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	// generating the light cone mesh
	{
		tsl::IndexedMesh mesh;
		tsl::CreateCone(1.f, 1.f, 12, &mesh);
		tsl::ConvertPolygonsToTriangles(&mesh);

		light_cone_mesh_.element_count = mesh.index_array.size();

		glGenBuffers(1, &light_cone_mesh_.position_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, light_cone_mesh_.position_vbo);
		glBufferData(GL_ARRAY_BUFFER,
			mesh.vertex_array.size() * sizeof(glm::vec3),
			mesh.vertex_array.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &light_cone_mesh_.element_vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_cone_mesh_.element_vbo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			mesh.index_array.size() * sizeof(unsigned int),
			mesh.index_array.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glGenVertexArrays(1, &light_cone_mesh_.vao);
		glBindVertexArray(light_cone_mesh_.vao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_cone_mesh_.element_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, light_cone_mesh_.position_vbo);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
			sizeof(glm::vec3), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	//generate the defer program
	{
		GLint compile_status = 0;
		// Create the vertex shader
		GLuint defer_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		std::string defer_vertex_shader_string = tygra::stringFromFile("defer_vs.glsl");
		const char *defer_vertex_shader_code = defer_vertex_shader_string.c_str();
		glShaderSource(defer_vertex_shader, 1,
			(const GLchar **)&defer_vertex_shader_code, NULL);
		glCompileShader(defer_vertex_shader);
		glGetShaderiv(defer_vertex_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(defer_vertex_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		// Create the fragment shader
		GLuint defer_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		std::string defer_fragment_shader_string = tygra::stringFromFile("defer_fs.glsl");
		const char *defer_fragment_shader_code = defer_fragment_shader_string.c_str();
		glShaderSource(defer_fragment_shader, 1,
			(const GLchar **)&defer_fragment_shader_code, NULL);
		glCompileShader(defer_fragment_shader);
		glGetShaderiv(defer_fragment_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(defer_fragment_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
		// Bind uniform variables to the vertex shader
		defer_program_ = glCreateProgram();
		glAttachShader(defer_program_, defer_vertex_shader);
		glBindAttribLocation(defer_program_, 0, "vertexPosition");
		glBindAttribLocation(defer_program_, 1, "vertexNormal");
		glBindAttribLocation(defer_program_, 2, "tex_coord");
		glDeleteShader(defer_vertex_shader);

		// Bind uniform variables to the fragment shader
		glAttachShader(defer_program_, defer_fragment_shader);
		glBindFragDataLocation(defer_program_, 0, "fragment_positions");
		glBindFragDataLocation(defer_program_, 1, "fragment_normals");
		glBindFragDataLocation(defer_program_, 2, "fragment_materials");
		glDeleteShader(defer_fragment_shader);
		glLinkProgram(defer_program_);

		GLint link_status = 0;
		glGetProgramiv(defer_program_, GL_LINK_STATUS, &link_status);
		if (link_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetProgramInfoLog(defer_program_, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
	}

	{
		GLint compile_status = 0;

		GLuint global_light_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		std::string global_light_vertex_shader_string
			= tygra::stringFromFile("global_light_vs.glsl");
		const char *global_light_vertex_shader_code = global_light_vertex_shader_string.c_str();
		glShaderSource(global_light_vertex_shader, 1,
			(const GLchar **)&global_light_vertex_shader_code, NULL);
		glCompileShader(global_light_vertex_shader);
		glGetShaderiv(global_light_vertex_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(global_light_vertex_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		GLuint global_light_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		std::string global_light_fragment_shader_string =
			tygra::stringFromFile("global_light_fs.glsl");
		const char *global_light_fragment_shader_code = global_light_fragment_shader_string.c_str();
		glShaderSource(global_light_fragment_shader, 1,
			(const GLchar **)&global_light_fragment_shader_code, NULL);
		glCompileShader(global_light_fragment_shader);
		glGetShaderiv(global_light_fragment_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(global_light_fragment_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		global_light_prog_ = glCreateProgram();
		glAttachShader(global_light_prog_, global_light_vertex_shader);
		glBindAttribLocation(global_light_prog_, 0, "vertex_position");
		glBindAttribLocation(global_light_prog_, 1, "normal_position");
		glDeleteShader(global_light_vertex_shader);

		glAttachShader(global_light_prog_, global_light_fragment_shader);
		glBindFragDataLocation(global_light_prog_, 0, "reflected_light");
		glDeleteShader(global_light_fragment_shader);
		glLinkProgram(global_light_prog_);

		GLint link_status = 0;
		glGetProgramiv(global_light_prog_, GL_LINK_STATUS, &link_status);
		if (link_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetProgramInfoLog(global_light_prog_, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
	}

	// generate the point light program
	{
		GLint compile_status = 0;
		GLuint point_light_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		std::string point_light_vertex_shader_string
			= tygra::stringFromFile("point_light_vs.glsl");
		const char *point_light_vertex_shader_code = point_light_vertex_shader_string.c_str();
		glShaderSource(point_light_vertex_shader, 1,
			(const GLchar **)&point_light_vertex_shader_code, NULL);
		glCompileShader(point_light_vertex_shader);
		glGetShaderiv(point_light_vertex_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(point_light_vertex_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		GLuint point_light_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		std::string point_light_fragment_shader_string =
			tygra::stringFromFile("point_light_fs.glsl");
		const char *point_light_fragment_shader_code = point_light_fragment_shader_string.c_str();
		glShaderSource(point_light_fragment_shader, 1,
			(const GLchar **)&point_light_fragment_shader_code, NULL);
		glCompileShader(point_light_fragment_shader);
		glGetShaderiv(point_light_fragment_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(point_light_fragment_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		point_light_prog_ = glCreateProgram();
		glAttachShader(point_light_prog_, point_light_vertex_shader);
		glBindAttribLocation(point_light_prog_, 0, "vertex_position");
		glDeleteShader(point_light_vertex_shader);
		glAttachShader(point_light_prog_, point_light_fragment_shader);
		glBindFragDataLocation(point_light_prog_, 0, "reflected_light");
		glDeleteShader(point_light_fragment_shader);
		glLinkProgram(point_light_prog_);

		GLint link_status = 0;
		glGetProgramiv(point_light_prog_, GL_LINK_STATUS, &link_status);
		if (link_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetProgramInfoLog(point_light_prog_, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
	}
	// generate the spot light program
	{
		GLint compile_status = 0;
		GLuint spot_light_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		std::string spot_light_vertex_shader_string
			= tygra::stringFromFile("spot_light_vs.glsl");
		const char *spot_light_vertex_shader_code = spot_light_vertex_shader_string.c_str();
		glShaderSource(spot_light_vertex_shader, 1,
			(const GLchar **)&spot_light_vertex_shader_code, NULL);
		glCompileShader(spot_light_vertex_shader);
		glGetShaderiv(spot_light_vertex_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(spot_light_vertex_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		GLuint spot_light_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		std::string spot_light_fragment_shader_string =
			tygra::stringFromFile("spot_light_fs.glsl");
		const char *spot_light_fragment_shader_code = spot_light_fragment_shader_string.c_str();
		glShaderSource(spot_light_fragment_shader, 1,
			(const GLchar **)&spot_light_fragment_shader_code, NULL);
		glCompileShader(spot_light_fragment_shader);
		glGetShaderiv(spot_light_fragment_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(spot_light_fragment_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		spot_light_prog_ = glCreateProgram();
		glAttachShader(spot_light_prog_, spot_light_vertex_shader);
		glBindAttribLocation(spot_light_prog_, 0, "vertex_position");
		glDeleteShader(spot_light_vertex_shader);
		glAttachShader(spot_light_prog_, spot_light_fragment_shader);
		glBindFragDataLocation(spot_light_prog_, 0, "reflected_light");
		glDeleteShader(spot_light_fragment_shader);
		glLinkProgram(spot_light_prog_);

		GLint link_status = 0;
		glGetProgramiv(spot_light_prog_, GL_LINK_STATUS, &link_status);
		if (link_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetProgramInfoLog(spot_light_prog_, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
	}
	// generate the post-process program
	{
		GLint compile_status = 0;
		GLuint post_process_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		std::string post_process_vertex_shader_string
			= tygra::stringFromFile("post_process_vs.glsl");
		const char *post_process_vertex_shader_code = post_process_vertex_shader_string.c_str();
		glShaderSource(post_process_vertex_shader, 1,
			(const GLchar **)&post_process_vertex_shader_code, NULL);
		glCompileShader(post_process_vertex_shader);
		glGetShaderiv(post_process_vertex_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(post_process_vertex_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		GLuint post_process_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		std::string post_process_fragment_shader_string =
			tygra::stringFromFile("post_process_fs.glsl");
		const char *post_process_fragment_shader_code = post_process_fragment_shader_string.c_str();
		glShaderSource(post_process_fragment_shader, 1,
			(const GLchar **)&post_process_fragment_shader_code, NULL);
		glCompileShader(post_process_fragment_shader);
		glGetShaderiv(post_process_fragment_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(post_process_fragment_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		post_process_prog_ = glCreateProgram();
		glAttachShader(post_process_prog_, post_process_vertex_shader);
		glBindAttribLocation(post_process_prog_, 0, "vertex_position");
		glDeleteShader(post_process_vertex_shader);
		glAttachShader(post_process_prog_, post_process_fragment_shader);
		glBindFragDataLocation(post_process_prog_, 0, "reflected_light");
		glDeleteShader(post_process_fragment_shader);
		glLinkProgram(post_process_prog_);

		GLint link_status = 0;
		glGetProgramiv(post_process_prog_, GL_LINK_STATUS, &link_status);
		if (link_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetProgramInfoLog(post_process_prog_, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
	}

	{
		GLint compile_status = 0;

		GLuint shadow_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		std::string shadow_vertex_shader_string
			= tygra::stringFromFile("shadow_vs.glsl");
		const char *shadow_vertex_shader_code = shadow_vertex_shader_string.c_str();
		glShaderSource(shadow_vertex_shader, 1,
			(const GLchar **)&shadow_vertex_shader_code, NULL);
		glCompileShader(shadow_vertex_shader);
		glGetShaderiv(shadow_vertex_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(shadow_vertex_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		GLuint shadow_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		std::string shadow_fragment_shader_string =
			tygra::stringFromFile("shadow_fs.glsl");
		const char *shadow_fragment_shader_code = shadow_fragment_shader_string.c_str();
		glShaderSource(shadow_fragment_shader, 1,
			(const GLchar **)&shadow_fragment_shader_code, NULL);
		glCompileShader(shadow_fragment_shader);
		glGetShaderiv(shadow_fragment_shader, GL_COMPILE_STATUS, &compile_status);
		if (compile_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetShaderInfoLog(shadow_fragment_shader, string_length, NULL, log);
			std::cerr << log << std::endl;
		}

		shadow_prog_ = glCreateProgram();
		glAttachShader(shadow_prog_, shadow_vertex_shader);
		glBindAttribLocation(shadow_prog_, 0, "vertex_position");
		glBindAttribLocation(shadow_prog_, 1, "vertex_normal");
		glDeleteShader(shadow_vertex_shader);

		glAttachShader(shadow_prog_, shadow_fragment_shader);
		glBindFragDataLocation(shadow_prog_, 0, "shadow_depth");
		glDeleteShader(shadow_fragment_shader);
		glLinkProgram(shadow_prog_);

		GLint link_status = 0;
		glGetProgramiv(shadow_prog_, GL_LINK_STATUS, &link_status);
		if (link_status != GL_TRUE) {
			const int string_length = 1024;
			GLchar log[string_length] = "";
			glGetProgramInfoLog(shadow_prog_, string_length, NULL, log);
			std::cerr << log << std::endl;
		}
	}

	{
		glGenFramebuffers(1, &lbuffer_fbo_);
		glGenFramebuffers(1, &gbuffer_fbo_);
		glGenFramebuffers(1, &shadow_fbo_);
		glGenRenderbuffers(1, &lbuffer_colour_rbo_);
		glGenRenderbuffers(1, &gbuffer_depth_rbo_);

		glGenTextures(1, &gbuffer_depth_tex_);
		glGenTextures(1, &gbuffer_position_tex_);
		glGenTextures(1, &gbuffer_normal_tex_);
		glGenTextures(1, &gbuffer_material_tex_);
		glGenTextures(1, &fbo_texture_);
		glGenTextures(1, &shadow_tex_);
	}

	{
		glGenBuffers(1, &per_model_ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, per_model_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(PerModelUniforms), nullptr, GL_STREAM_DRAW);

		glBindBufferBase(GL_UNIFORM_BUFFER, 0, per_model_ubo);
		glUniformBlockBinding(defer_program_, glGetUniformBlockIndex(defer_program_, "PerModelUniforms"), 0);

		glGenBuffers(1, &per_directional_light_ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, per_directional_light_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(PerPointLightUniforms), nullptr, GL_STREAM_DRAW);

		glBindBufferBase(GL_UNIFORM_BUFFER, 1, per_directional_light_ubo);
		glUniformBlockBinding(global_light_prog_, glGetUniformBlockIndex(global_light_prog_, "PerDirectionalLightUniforms"), 1);

		glGenBuffers(1, &per_point_light_ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, per_point_light_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(PerPointLightUniforms), nullptr, GL_STREAM_DRAW);

		glBindBufferBase(GL_UNIFORM_BUFFER, 2, per_point_light_ubo);
		glUniformBlockBinding(point_light_prog_, glGetUniformBlockIndex(point_light_prog_, "PerPointLightUniforms"), 2);

		glGenBuffers(1, &per_spot_light_ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, per_spot_light_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(PerSpotLightUniforms), nullptr, GL_STREAM_DRAW);

		glBindBufferBase(GL_UNIFORM_BUFFER, 3, per_spot_light_ubo);
		glUniformBlockBinding(spot_light_prog_, glGetUniformBlockIndex(spot_light_prog_, "PerSpotLightUniforms"), 3);
	}

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Build the scene
	SceneModel::GeometryBuilder builder;
	const auto& scene_meshes = builder.getAllMeshes();
	for (SceneModel::Mesh scene_mesh : scene_meshes)
	{
		MyView::MeshGL& newMesh = meshes_[scene_mesh.getId()];
		newMesh = BuildMesh(scene_mesh);
	}

	assert(scene_ != nullptr);
}

void MyView::
windowViewDidReset(std::shared_ptr<tygra::Window> window,
int width,
int height)
{
	window_width = width;
	window_height = height;

	glBindTexture(GL_TEXTURE_RECTANGLE, fbo_texture_);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, lbuffer_colour_rbo_);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB8, width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, gbuffer_depth_rbo_);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex_);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex_);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_material_tex_);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glBindTexture(GL_TEXTURE_2D, shadow_tex_);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	{
		GLenum framebuffer_status = 0;

		glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo_);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, gbuffer_depth_rbo_);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, gbuffer_position_tex_, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_RECTANGLE, gbuffer_normal_tex_, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_RECTANGLE, gbuffer_material_tex_, 0);

		framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (framebuffer_status != GL_FRAMEBUFFER_COMPLETE) {
			tglDebugMessage(GL_DEBUG_SEVERITY_HIGH_ARB, "Gbuffer not complete");
		}

		GLenum buffers[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
		glDrawBuffers(3, buffers);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	{
		GLenum framebuffer_status = 0;

		glBindFramebuffer(GL_FRAMEBUFFER, lbuffer_fbo_);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, fbo_texture_, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, gbuffer_depth_rbo_);

		framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (framebuffer_status != GL_FRAMEBUFFER_COMPLETE) {
			tglDebugMessage(GL_DEBUG_SEVERITY_HIGH_ARB, "Lbuffer not complete");
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	{
		GLenum framebuffer_status = 0;

		glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo_);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadow_tex_, 0);

		framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (framebuffer_status != GL_FRAMEBUFFER_COMPLETE) {
			tglDebugMessage(GL_DEBUG_SEVERITY_HIGH_ARB, "Lbuffer not complete");
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	glViewport(0, 0, width, height);
}

void MyView::
windowViewDidStop(std::shared_ptr<tygra::Window> window)
{
	// Delete all the VBOs and the VAO
	glDeleteProgram(defer_program_);
	glDeleteProgram(global_light_prog_);
	glDeleteProgram(point_light_prog_);
	glDeleteProgram(spot_light_prog_);
	glDeleteProgram(post_process_prog_);
	glDeleteProgram(shadow_prog_);

	glDeleteBuffers(1, &light_quad_mesh_.position_vbo);
	glDeleteBuffers(1, &light_quad_mesh_.element_vbo);
	glDeleteVertexArrays(1, &light_quad_mesh_.vao);

	glDeleteBuffers(1, &light_sphere_mesh_.position_vbo);
	glDeleteBuffers(1, &light_sphere_mesh_.element_vbo);
	glDeleteVertexArrays(1, &light_sphere_mesh_.vao);

	glDeleteBuffers(1, &light_cone_mesh_.position_vbo);
	glDeleteBuffers(1, &light_cone_mesh_.element_vbo);
	glDeleteVertexArrays(1, &light_cone_mesh_.vao);

	glDeleteBuffers(1, &mesh_.position_vbo);
	glDeleteBuffers(1, &mesh_.element_vbo);
	glDeleteVertexArrays(1, &mesh_.vao);

	glDeleteTextures(1, &gbuffer_position_tex_);
	glDeleteTextures(1, &gbuffer_normal_tex_);
	glDeleteTextures(1, &gbuffer_depth_tex_);
	glDeleteTextures(1, &gbuffer_material_tex_);
	glDeleteTextures(1, &fbo_texture_);
	glDeleteTextures(1, &shadow_tex_);

	glDeleteFramebuffers(1, &lbuffer_fbo_);
	glDeleteFramebuffers(1, &gbuffer_fbo_);
	glDeleteFramebuffers(1, &shadow_fbo_);
}

void MyView::
windowViewRender(std::shared_ptr<tygra::Window> window)
{
	assert(scene_ != nullptr);

	glUseProgram(defer_program_);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);

	// config stencil
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_ALWAYS, 0, ~0);
	glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

	glStencilMask(0XFF);

	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo_);
	glClearStencil(128);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	const float angle_radians = 3.14;
	const float radius = 0.25f;
	GLint viewport_size[4];
	glGetIntegerv(GL_VIEWPORT, viewport_size);
	const float aspect_ratio
		= viewport_size[2] / (float)viewport_size[3];

	const SceneModel::Camera camera = scene_->getCamera();

	// Define all the matrices to be used in the shaders
	glm::mat4 projection_xform = glm::perspective(75.f,
		aspect_ratio,
		1.f, 1000.f);

	glm::mat4 view_xform = glm::lookAt(camera.getPosition(), camera.getDirection() + camera.getPosition(), glm::vec3(0, 1, 0));

	glm::mat4 projection_view_xform = projection_xform * view_xform;

	glm::mat4 model_xform;

	// Pass the camera position to the shaders
	glm::vec3 camera_position = camera.getPosition();
	GLuint cam_pos = glGetUniformLocation(defer_program_, "camera_position");
	glUniform3f(cam_pos, camera_position.x, camera_position.y, camera_position.z);

	PerModelUniforms per_model_uniforms;

	// Loop through all of the meshes and add them to the scene
	for (const auto& instance : scene_->getAllInstances()) {

		// Apply transforms to the models
		glm::mat4 model_xform = glm::mat4(instance.getTransformationMatrix());
		const MeshGL& mesh = meshes_[instance.getMeshId()];

		per_model_uniforms.model_xform = model_xform;

		glm::mat4 combined_xform = projection_xform
			* view_xform
			* model_xform;

		per_model_uniforms.combined_xform = combined_xform;

		glBindBuffer(GL_UNIFORM_BUFFER, per_model_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(per_model_uniforms), &per_model_uniforms, GL_STREAM_DRAW);

		auto mat_id = instance.getMaterialId();
		auto& mat_mesh = scene_->getMaterialById(mat_id);

		// Pass the diffuse and specular colours to the scene
		glm::vec3 mat_colour = mat_mesh.getDiffuseColour() + mat_mesh.getSpecularColour();
		GLuint mat_col_id = glGetUniformLocation(defer_program_, "mat_colour");
		glUniform3f(mat_col_id, mat_colour.r, mat_colour.g, mat_colour.b);

		// Pass the shininess to the scene
		float shininess = mat_mesh.getShininess();
		GLuint shininess_id = glGetUniformLocation(defer_program_, "shininess");
		glUniform1f(shininess_id, 0);

		glBindVertexArray(meshes_[instance.getMeshId()].vao);
		glDrawElements(GL_TRIANGLES, meshes_[instance.getMeshId()].element_count, GL_UNSIGNED_INT, 0);
	}

	// draw quad
	glBindVertexArray(light_quad_mesh_.vao);

	// config stencil
	glStencilFunc(GL_NOTEQUAL, 128, ~0);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glEnable(GL_STENCIL_TEST);
	glStencilMask(GL_TRUE);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, lbuffer_fbo_);
	glClearColor(0.f, 0.f, 0.25f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(global_light_prog_);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex_);
	glUniform1i(glGetUniformLocation(global_light_prog_, "sampler_world_position"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex_);
	glUniform1i(glGetUniformLocation(global_light_prog_, "sampler_world_normal"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_material_tex_);
	glUniform1i(glGetUniformLocation(global_light_prog_, "sampler_world_material"), 2);

	model_xform = glm::mat4{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

	PerDirectionalLightUniforms per_directional_light_uniforms;

	for (const auto& light : scene_->getAllDirectionalLights())
	{
		glm::vec3 global_light_direction = light.getDirection();
		glm::vec3 global_light_intensity = light.getIntensity();

		per_directional_light_uniforms.direction = global_light_direction;
		per_directional_light_uniforms.intensity = global_light_intensity;

		glBindBuffer(GL_UNIFORM_BUFFER, per_directional_light_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(per_directional_light_uniforms), &per_directional_light_uniforms, GL_STREAM_DRAW);
	}
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	
	glUseProgram(point_light_prog_);

	glBindVertexArray(light_sphere_mesh_.vao);

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glDisable(GL_DEPTH_TEST);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex_);
	glUniform1i(glGetUniformLocation(point_light_prog_, "sampler_world_position"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex_);
	glUniform1i(glGetUniformLocation(point_light_prog_, "sampler_world_normal"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_material_tex_);
	glUniform1i(glGetUniformLocation(point_light_prog_, "sampler_world_material"), 2);

	PerPointLightUniforms per_point_light_uniforms;

	for (const auto& point_light : scene_->getAllPointLights())
	{
		glm::vec3 point_light_position = point_light.getPosition();
		glm::vec3 point_light_intensity = point_light.getIntensity();

		float point_light_range = point_light.getRange();

		glm::mat4 model_xform_ = glm::mat4(glm::translate(glm::mat4(), point_light_position) * glm::scale(glm::mat4(), glm::vec3(point_light_range)));

		per_point_light_uniforms.model_xform = model_xform_;

		glm::mat4 combined_xform = projection_view_xform * model_xform_;

		per_point_light_uniforms.combined_xform = combined_xform;

		glUniform3fv(glGetUniformLocation(point_light_prog_, "camera_position"), 1, glm::value_ptr(camera_position));

		per_point_light_uniforms.position = point_light_position;
		per_point_light_uniforms.intensity = point_light_intensity;
		per_point_light_uniforms.range = point_light_range;

		glBindBuffer(GL_UNIFORM_BUFFER, per_point_light_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(per_point_light_uniforms), &per_point_light_uniforms, GL_STREAM_DRAW);

		glDrawElements(GL_TRIANGLES, light_sphere_mesh_.element_count, GL_UNSIGNED_INT, 0);
	}
	
	float orthosize = 10.0f;

	glEnable(GL_DEPTH_TEST);
	glUseProgram(shadow_prog_);
	glViewport(0, 0, 1024, 1024);
	glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo_);
	glClear(GL_DEPTH_BUFFER_BIT);

	for (const auto& spot_light : scene_->getAllSpotLights())
	{
		glm::vec3 spot_light_position = spot_light.getPosition();
		glm::vec3 spot_light_intensity = spot_light.getIntensity();
		glm::vec3 spot_light_direction = spot_light.getDirection();
		float spot_light_cone_angle = spot_light.getConeAngleDegrees();

		float spot_light_range = spot_light.getRange();

		float r = spot_light_range * tanf(0.5f * spot_light_cone_angle * 3.14f / 180.0f);

		glm::mat4 depth_projection_xform = glm::ortho(-orthosize, orthosize, -orthosize, orthosize, 0.0f, spot_light_range);
		glm::mat4 depth_view_xform = glm::lookAt(spot_light_position, spot_light_position + spot_light_direction, scene_->getUpDirection());

		for (SceneModel::Instance instance : scene_->getAllInstances())
		{
			glm::mat4 model_xform = glm::mat4(instance.getTransformationMatrix());
			glm::mat4 combined_xform = depth_projection_xform * depth_view_xform * model_xform;

			glUniformMatrix4fv(glGetUniformLocation(shadow_prog_, "combined_xform"), 1, GL_FALSE, glm::value_ptr(combined_xform));

			glBindVertexArray(meshes_[instance.getMeshId()].vao);
			glDrawElements(GL_TRIANGLES, meshes_[instance.getMeshId()].element_count, GL_UNSIGNED_INT, 0);
		}

		break;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, lbuffer_fbo_);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, window_width, window_height);
	glUseProgram(spot_light_prog_);
	glBindVertexArray(light_cone_mesh_.vao);

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glDisable(GL_DEPTH_TEST);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex_);
	glUniform1i(glGetUniformLocation(spot_light_prog_, "sampler_world_position"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex_);
	glUniform1i(glGetUniformLocation(spot_light_prog_, "sampler_world_normal"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_material_tex_);
	glUniform1i(glGetUniformLocation(spot_light_prog_, "sampler_world_material"), 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, shadow_tex_);
	glUniform1i(glGetUniformLocation(spot_light_prog_, "shadow_sampler"), 3);

	PerSpotLightUniforms per_spot_light_uniforms;

	for (const auto& spot_light : scene_->getAllSpotLights())
	{
		glm::vec3 spot_light_position = spot_light.getPosition();
		glm::vec3 spot_light_intensity = spot_light.getIntensity();
		glm::vec3 spot_light_direction = spot_light.getDirection();
		float spot_light_cone_angle = spot_light.getConeAngleDegrees();

		float spot_light_range = spot_light.getRange();

		float r = spot_light_range * tanf(0.5f * spot_light_cone_angle * 3.14f / 180.0f);

		glm::mat4 model_xform_ = glm::inverse(glm::lookAt(spot_light_position, spot_light_position + spot_light_direction, glm::vec3(0, 1, 0)))
			* glm::scale(glm::mat4(), glm::vec3(r, r, spot_light_range))
			* glm::translate(glm::mat4(), glm::vec3(0, 0, -1));

		glm::mat4 combined_xform = projection_view_xform * model_xform_;

		glm::mat4 depth_projection_xform = glm::ortho(-orthosize, orthosize, -orthosize, orthosize, 0.0f, spot_light_range);
		glm::mat4 depth_view_xform = glm::lookAt(spot_light_position, spot_light_position + spot_light_direction, scene_->getUpDirection());

		glUniform3fv(glGetUniformLocation(spot_light_prog_, "camera_position"), 1, glm::value_ptr(camera_position));

		glm::vec3 camera_direction = camera.getDirection();
		GLuint cam_pos = glGetUniformLocation(spot_light_prog_, "camera_direction");
		glUniform3f(cam_pos, camera_position.x, camera_position.y, camera_position.z);

		glUniform3fv(glGetUniformLocation(spot_light_prog_, "camera_direction"), 1, glm::value_ptr(camera_direction));

		glUniformMatrix4fv(glGetUniformLocation(spot_light_prog_, "depth_projection_xform"), 1, GL_FALSE, glm::value_ptr(depth_projection_xform));
		glUniformMatrix4fv(glGetUniformLocation(spot_light_prog_, "depth_view_xform"), 1, GL_FALSE, glm::value_ptr(depth_view_xform));

		per_spot_light_uniforms.combined_xform = combined_xform;
		per_spot_light_uniforms.model_xform = model_xform_;

		per_spot_light_uniforms.position = spot_light_position;
		per_spot_light_uniforms.range = spot_light_range;
		per_spot_light_uniforms.intensity = spot_light_intensity;
		per_spot_light_uniforms.cone_angle = spot_light_cone_angle;
		per_spot_light_uniforms.direction = spot_light_direction;

		glBindBuffer(GL_UNIFORM_BUFFER, per_spot_light_ubo);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(per_spot_light_uniforms), &per_spot_light_uniforms, GL_STREAM_DRAW);

		glDrawElements(GL_TRIANGLES, light_cone_mesh_.element_count, GL_UNSIGNED_INT, 0);
	}

	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_BLEND);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, lbuffer_fbo_);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glUseProgram(post_process_prog_);

	glBindVertexArray(light_quad_mesh_.vao);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, fbo_texture_);
	glUniform1i(glGetUniformLocation(post_process_prog_, "fbo_texture"), 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

// build the meshes into the scene
MyView::MeshGL MyView::BuildMesh(const SceneModel::Mesh& source_mesh)
{
	const auto& positions = source_mesh.getPositionArray();
	const auto& elements = source_mesh.getElementArray();
	const auto& normal = source_mesh.getNormalArray();
	const auto& texCoord = source_mesh.getTextureCoordinateArray();

	// Position Buffer
	glGenBuffers(1, &mesh_.position_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, mesh_.position_vbo);
	glBufferData(GL_ARRAY_BUFFER,
		positions.size() * sizeof(glm::vec3), // size of data in bytes
		positions.data(), // pointer to the data
		GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Normal Buffer
	glGenBuffers(1, &mesh_.normal_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, mesh_.normal_vbo);
	glBufferData(GL_ARRAY_BUFFER,
		normal.size() * sizeof(glm::vec3),
		normal.data(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture Buffer
	glGenBuffers(1, &mesh_.texCoord_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, mesh_.texCoord_vbo);
	glBufferData(GL_ARRAY_BUFFER, texCoord.size() * sizeof(glm::vec2), texCoord.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Element Buffer
	glGenBuffers(1, &mesh_.element_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_.element_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		elements.size() * sizeof(unsigned int),
		elements.data(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	mesh_.element_count = elements.size();

	glGenVertexArrays(1, &mesh_.vao);
	glBindVertexArray(mesh_.vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_.element_vbo);

	glBindBuffer(GL_ARRAY_BUFFER, mesh_.position_vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(glm::vec3), TGL_BUFFER_OFFSET(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, mesh_.normal_vbo);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		sizeof(glm::vec3), TGL_BUFFER_OFFSET(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, mesh_.texCoord_vbo);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		sizeof(glm::vec2), TGL_BUFFER_OFFSET(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return mesh_;
}