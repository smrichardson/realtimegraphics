#pragma once

#include <SceneModel/SceneModel_fwd.hpp>
#include <tygra/WindowViewDelegate.hpp>
#include <tgl/tgl.h>
#include <glm/glm.hpp>
#include <map>
#include <vector>
#include <memory>

class MyView : public tygra::WindowViewDelegate
{
public:

    MyView();

    ~MyView();

    void
    setScene(std::shared_ptr<const SceneModel::Context> scene);
private:

    void
    windowViewWillStart(std::shared_ptr<tygra::Window> window) override;

    void
    windowViewDidReset(std::shared_ptr<tygra::Window> window,
                       int width,
                       int height) override;

    void
    windowViewDidStop(std::shared_ptr<tygra::Window> window) override;

    void
    windowViewRender(std::shared_ptr<tygra::Window> window) override;

    std::shared_ptr<const SceneModel::Context> scene_;
	
	struct MeshGL
	{
		GLuint texCoord_vbo;
		GLuint normal_vbo;
		GLuint position_vbo; // VertexBufferObject for the vertex positions
		GLuint element_vbo; // VertexBufferObject for the elements (indices)
		GLuint vao; // VertexArrayObject for the shape's vertex array settings
		int element_count; // Needed for when we draw using the vertex arrays

		MeshGL() : position_vbo(0),
			normal_vbo(0),
			element_vbo(0),
			texCoord_vbo(0),
			vao(0),
			element_count(0) {}
	};

	struct PerModelUniforms
	{
		glm::mat4 combined_xform;
		glm::mat4 model_xform;
	};
	GLuint per_model_ubo;

	struct PerDirectionalLightUniforms
	{
		glm::vec3 direction;
		glm::vec3 intensity;
	};
	GLuint per_directional_light_ubo;

	struct PerPointLightUniforms
	{
		glm::mat4 combined_xform;
		glm::mat4 model_xform;

		glm::vec3 position;
		float range;
		glm::vec3 intensity;	
	};
	GLuint per_point_light_ubo;
	
	struct PerSpotLightUniforms
	{
		glm::mat4 combined_xform;
		glm::mat4 model_xform;

		glm::vec3 position;
		float range;
		glm::vec3 intensity;
		float cone_angle;
		glm::vec3 direction;
	};
	GLuint per_spot_light_ubo;

	std::map<SceneModel::MeshId, MeshGL> meshes_;

	MeshGL BuildMesh(const SceneModel::Mesh& source_mesh);
	MeshGL mesh_;
	MeshGL light_quad_mesh_;
	MeshGL light_sphere_mesh_;
	MeshGL light_cone_mesh_;

	GLuint lbuffer_fbo_;
	GLuint gbuffer_fbo_;
	GLuint shadow_fbo_;
	GLuint lbuffer_colour_rbo_;
	GLuint gbuffer_depth_rbo_;	

	GLuint gbuffer_position_tex_;
	GLuint gbuffer_normal_tex_;
	GLuint gbuffer_depth_tex_;
	GLuint gbuffer_material_tex_;
	GLuint fbo_texture_;
	GLuint shadow_tex_;

	GLuint defer_program_;
	GLuint global_light_prog_;
	GLuint point_light_prog_;
	GLuint spot_light_prog_;
	GLuint post_process_prog_;
	GLuint shadow_prog_;

	int window_width;
	int window_height;
};
