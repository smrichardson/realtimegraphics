#version 330

uniform sampler2DRect sampler_world_position;
uniform sampler2DRect sampler_world_normal;
uniform sampler2DRect sampler_world_material;

layout (std140) uniform PerDirectionalLightUniforms 
{
	vec3 direction;
	vec3 intensity;
} UNIFORMS;

out vec3 reflected_light;

void main(void)
{
	vec3 texel_P = texelFetch(sampler_world_position, ivec2(gl_FragCoord.xy)).rgb;
	vec3 texel_N = texelFetch(sampler_world_normal, ivec2(gl_FragCoord.xy)).rgb;
	vec3 texel_M = texelFetch(sampler_world_material, ivec2(gl_FragCoord.xy)).rgb;
	
	vec3 N = normalize(texel_N);
	vec3 L = normalize(-UNIFORMS.direction);
	
	float diffuseIntensity = max(dot(L, N),0);
	vec3 diffuseLight = UNIFORMS.intensity * diffuseIntensity;
	reflected_light = diffuseLight;
}
