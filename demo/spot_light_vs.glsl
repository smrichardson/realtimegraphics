#version 330

layout(std140) uniform PerSpotLightUniforms
{
	mat4 combined_xform;
	mat4 model_xform;

	vec3 position;
	float range;
	vec3 intensity;
	float cone_angle;
	vec3 direction;
}TRANSFORMS;

uniform mat4 depth_projection_xform;
uniform mat4 depth_view_xform;

in vec3 vertex_position;

out vec4 shadow_tex_coord;

void main(void)
{
    gl_Position = TRANSFORMS.combined_xform * vec4(vertex_position, 1.0);
	
	shadow_tex_coord = (depth_projection_xform * depth_view_xform) * (TRANSFORMS.model_xform * vec4(vertex_position, 1.0));
}