#version 330

uniform vec3 mat_colour;
uniform float shininess;

in vec3 P;
in vec3 N;
in vec4 tex_pos;

out vec3 fragment_positions;
out vec3 fragment_normals;
out vec4 fragment_materials;

void main(void)
{
	fragment_positions = P;
	fragment_normals = N;
	
	fragment_materials = vec4(mat_colour, shininess);
}