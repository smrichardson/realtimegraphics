/*
    The render algorithm is:
        ambient render; foreach light { shadow map render; additive light render; }
    This fragment shader is for the shadow map render.
    You must output a value to the shadow map to complete this shader.
    The shadow map texture is a RGB32F for maximum flexibility in your experiments.
*/


#version 330

in vec3 varying_position;
in vec3 varying_normal;

out vec3 shadow_depth;

void main(void)
{
	gl_FragDepth = gl_FragCoord.z;
}
