#version 330

uniform sampler2DRect sampler_world_position;
uniform sampler2DRect sampler_world_normal;
uniform sampler2DRect sampler_world_material;

uniform sampler2D shadow_sampler;

uniform vec3 camera_position;
uniform vec3 camera_direction;

layout(std140) uniform PerSpotLightUniforms
{
	mat4 combined_xform;
	mat4 model_xform;

	vec3 position;
	float range;
	vec3 intensity;
	float cone_angle;
	vec3 direction;
}UNIFORMS;

in vec4 shadow_tex_coord;

out vec3 reflected_light;

void main(void)
{
	vec3 texel_P = texelFetch(sampler_world_position, ivec2(gl_FragCoord.xy)).rgb;
	vec3 texel_N = texelFetch(sampler_world_normal, ivec2(gl_FragCoord.xy)).rgb;
	vec3 texel_M = texelFetch(sampler_world_material, ivec2(gl_FragCoord.xy)).rgb;
	float texel_S = texelFetch(sampler_world_material, ivec2(gl_FragCoord.xy)).a;
	
	float distance = distance(texel_P, UNIFORMS.position);
	float attenuation = smoothstep(0, distance, 20);
	
	vec3 N = normalize(texel_N);
	vec3 L = normalize(UNIFORMS.position - texel_P);
	vec3 V = normalize(camera_position - texel_P);
	vec3 R = normalize(-reflect(L,N));
	
	float light_angle = dot(-L, UNIFORMS.direction);
	float outer_cone_angle = radians(UNIFORMS.cone_angle/2);
	float angle = acos(light_angle);
	
	float spot_effect = angle > outer_cone_angle ? 0 : 1.0 - angle / outer_cone_angle;
			
	vec3 specular = dot(R, V) * vec3(0.5,0.5,0.5) * texel_S;
			
	float diffuseIntensity = max(dot(L, N),0);
	vec3 diffuseLight =  UNIFORMS.intensity * diffuseIntensity;
			
	diffuseLight = clamp(diffuseLight, 0.0, 1.0);
			
	reflected_light = texel_M*(diffuseLight + specular) * spot_effect * attenuation;
}
