#version 330

layout(std140) uniform PerPointLightUniforms
{
	mat4 combined_xform;
	mat4 model_xform;

	vec3 position;
	float range;
	vec3 intensity;
}TRANSFORMS;

in vec3 vertex_position;

void main(void)
{
	gl_Position = TRANSFORMS.combined_xform * vec4(vertex_position, 1.0);
}