#version 330

#define FXAA_EDGE_THRESHOLD 		(1/16)
#define FXAA_EDGE_THRESHOLD_MIN 	(1/16) 

#define FXAA_SUBPIX 				1
#define FXAA_SUBPIX_TRIM 			(1/8)
#define FXAA_SUBPIX_CAP 			1

#define FXAA_SEARCH_STEPS 			1
#define FXAA_SEARCH_ACCELERATION 	1
#define FXAA_SEARCH_THRESHOLD 		(1/4)

uniform sampler2DRect fbo_texture;

in vec2 varying_position;

out vec3 reflected_light;

float luminance(vec3 rgb)
{
	return rgb.y * (0.587/0.299) + rgb.x;
}

void main(void)
{
	vec3 rgbNorth = texelFetch(fbo_texture, ivec2(gl_FragCoord.x + 0, gl_FragCoord.y - 1)).rgb;
	vec3 rgbWest = texelFetch(fbo_texture, ivec2(gl_FragCoord.x - 1, gl_FragCoord.y + 0)).rgb;
	vec3 rgbMiddle = texelFetch(fbo_texture, ivec2(gl_FragCoord.x + 0, gl_FragCoord.y + 0)).rgb;
	vec3 rgbEast = texelFetch(fbo_texture, ivec2(gl_FragCoord.x + 1, gl_FragCoord.y + 0)).rgb;
	vec3 rgbSouth = texelFetch(fbo_texture, ivec2(gl_FragCoord.x + 0, gl_FragCoord.y + 1)).rgb;
	
	vec3 rgbL = rgbNorth + rgbWest + rgbMiddle + rgbEast + rgbSouth;
	
	vec3 rgbNorthWest = texelFetch(fbo_texture, ivec2(gl_FragCoord.x - 1, gl_FragCoord.y - 1)).rgb;
	vec3 rgbNorthEast = texelFetch(fbo_texture, ivec2(gl_FragCoord.x + 1, gl_FragCoord.y - 1)).rgb;
	vec3 rgbSouthWest = texelFetch(fbo_texture, ivec2(gl_FragCoord.x - 1, gl_FragCoord.y + 1)).rgb;
	vec3 rgbSouthEast = texelFetch(fbo_texture, ivec2(gl_FragCoord.x + 1, gl_FragCoord.y + 1)).rgb;
	
	rgbL += (rgbNorthWest + rgbNorthEast + rgbSouthWest + rgbSouthEast);
	rgbL *= vec3(1.0/9.0);
	
	float lumaNorth = luminance(rgbNorth);
	float lumaWest = luminance(rgbWest);
	float lumaMiddle = luminance(rgbMiddle);
	float lumaEast = luminance(rgbEast);
	float lumaSouth = luminance(rgbSouth);
	
	float lumaNorthWest = luminance(rgbNorthWest);
	float lumaNorthEast = luminance(rgbNorthEast);
	float lumaSouthWest = luminance(rgbSouthWest);
	float lumaSouthEast = luminance(rgbSouthEast);
	
	float lumaL = (lumaNorth + lumaWest + lumaEast + lumaSouth) * 0.25;
	float rangeL = abs(lumaL - lumaMiddle);
	
	float rangeMin = min(lumaMiddle, min(min(lumaNorth, lumaWest), min(lumaSouth, lumaEast)));
	float rangeMax = max(lumaMiddle, max(max(lumaNorth, lumaWest), max(lumaSouth, lumaEast)));
	
	float range = rangeMax - rangeMin;
	
	float blendL = max(0.0, (rangeL / range) - FXAA_SUBPIX_TRIM) * FXAA_SUBPIX_TRIM;
	blendL = min(FXAA_SUBPIX_CAP, blendL);
	
	float edgeVert = 	abs((0.25 * lumaNorthWest) + (-0.5 * lumaNorth) + (0.25 * lumaNorthEast)) +
						abs((0.50 * lumaWest ) + (-1.0 * lumaMiddle) + (0.50 * lumaEast )) +
						abs((0.25 * lumaSouthWest) + (-0.5 * lumaSouth) + (0.25 * lumaSouthEast));

	float edgeHorz =	abs((0.25 * lumaNorthWest) + (-0.5 * lumaWest) + (0.25 * lumaSouthWest)) +
						abs((0.50 * lumaNorth ) + (-1.0 * lumaMiddle) + (0.50 * lumaSouth )) +
						abs((0.25 * lumaNorthEast) + (-0.5 * lumaEast) + (0.25 * lumaSouthEast));

	bool horzSpan = edgeHorz >= edgeVert;
	
	if(range < max(FXAA_EDGE_THRESHOLD_MIN, rangeMax * FXAA_EDGE_THRESHOLD) && horzSpan)
	{
		reflected_light =  rgbL;
	}
}