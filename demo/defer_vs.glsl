#version 330

layout (std140) uniform PerModelUniforms 
{
	mat4 combined_xform;
	mat4 model_xform;
} TRANSFORMS;


in vec3 vertex_position;
in vec3 vertex_normal;
in vec2 tex_coord;

out vec3 P;
out vec3 N;
out vec2 tex_pos;

void main(void)
{
	tex_pos = tex_coord;
	
	//create the position and normal transforms for the scene
	P = vec3(TRANSFORMS.model_xform*vec4(vertex_position,1.0));
	N = normalize(mat3(TRANSFORMS.model_xform)*vertex_normal);
    gl_Position = TRANSFORMS.combined_xform * vec4(vertex_position, 1.0);
}