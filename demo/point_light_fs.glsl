#version 330

uniform sampler2DRect sampler_world_position;
uniform sampler2DRect sampler_world_normal;
uniform sampler2DRect sampler_world_material;

uniform vec3 camera_position;

layout(std140) uniform PerPointLightUniforms
{
	mat4 combined_xform;
	mat4 model_xform;

	vec3 position;
	float range;
	vec3 intensity;
}UNIFORMS;

out vec3 reflected_light;

void main(void)
{
	vec3 texel_P = texelFetch(sampler_world_position, ivec2(gl_FragCoord.xy)).rgb;
	vec3 texel_N = texelFetch(sampler_world_normal, ivec2(gl_FragCoord.xy)).rgb;
	vec3 texel_M = texelFetch(sampler_world_material, ivec2(gl_FragCoord.xy)).rgb;
	float texel_S = texelFetch(sampler_world_material, ivec2(gl_FragCoord.xy)).a;
	
	vec3 N = normalize(texel_N);
	vec3 L = normalize(UNIFORMS.position - texel_P);
	
	vec3 V = normalize(camera_position - texel_P);
	vec3 R = normalize(reflect(-L,N));
	
	float distance = distance(UNIFORMS.position, texel_P);
	
	vec3 specular = dot(R, V) * vec3(1,1,1) * texel_S;
	
	float attenuation = 1.0 - smoothstep(0, UNIFORMS.range, distance);
	
	float diffuseIntensity = max(dot(L, N),1);
	vec3 diffuseLight = UNIFORMS.intensity * diffuseIntensity;
	reflected_light = (diffuseLight + specular) * attenuation;
}
