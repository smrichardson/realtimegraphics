#version 330

in vec2 vertex_position;

out vec2 varying_position;

void main(void)
{
	varying_position = vertex_position;

    gl_Position = vec4(vertex_position, 0.0, 1.0);
}